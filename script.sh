#!/bin/bash

TOKEN="MYTOKEN"
USER_CI="ceopsipa"
PROJECT_NAME="less2.1"
JOB_NAME="build_images"
API_BASE_URL="https://gitlab.com/api/v4"
CURL_CMD="curl -q -k --location --header PRIVATE-TOKEN:$TOKEN"

USER_ID=$($CURL_CMD $API_BASE_URL/users?username=$USER_CI | jq '. [] .id')
PROJECT_ID=$($CURL_CMD $API_BASE_URL/users/$USER_ID/projects | jq '. [] | select(.path == "'$PROJECT_NAME'") .id')
$CURL_CMD $API_BASE_URL/projects/$PROJECT_ID/jobs | jq '[.[] | select(.name == "'$JOB_NAME'" and .status == "failed")][0] .id'
